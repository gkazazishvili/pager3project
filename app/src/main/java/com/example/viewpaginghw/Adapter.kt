package com.example.viewpaginghw

import android.icu.text.Transliterator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.viewpaginghw.databinding.ItemBinding
import com.example.viewpaginghw.ui.main.MainFragment
import java.nio.file.attribute.PosixFileAttributeView

class Adapter(val context:MainFragment): PagingDataAdapter<Pager.Data, Adapter.ViewHolder>(DataDiff) {


    object DataDiff : DiffUtil.ItemCallback<Pager.Data>(){
        override fun areItemsTheSame(oldItem: Pager.Data, newItem: Pager.Data): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Pager.Data, newItem: Pager.Data): Boolean {
            return oldItem == newItem
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(getItem(position)!!, context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
         ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    class ViewHolder(private val binding:ItemBinding): RecyclerView.ViewHolder(binding.root){
        fun onBind(model:Pager.Data, context: MainFragment){
            val image:ImageView = binding.imageView
            Glide.with(context).load(model.avatar).
            into(binding.imageView)

            binding.email.text = model.email.toString()
            binding.firstname.text = model.firstName.toString()
            binding.lastname.text = model.lastName.toString()

        }
    }

}