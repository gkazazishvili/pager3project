package com.example.viewpaginghw

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.viewpaginghw.apiservice.ApiService
import com.example.viewpaginghw.ui.main.MainViewModel
import java.lang.IllegalArgumentException

class MainViewModelFactory(private val apiService: ApiService):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MainViewModel::class.java)){
            return MainViewModel(apiService) as T
        }
        throw IllegalArgumentException("not found view model")
    }

}