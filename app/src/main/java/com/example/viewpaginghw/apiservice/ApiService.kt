package com.example.viewpaginghw.apiservice

import android.provider.SyncStateContract
import com.example.viewpaginghw.Constant.Companion.BASE_URL
import com.example.viewpaginghw.Pager
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {


    @GET("users")
    suspend fun getImages(@Query("page") page: Int): Response<Pager>


    companion object{
        fun ok(): OkHttpClient {
            val builder = OkHttpClient().newBuilder()

            builder.addInterceptor(Interceptor {
                val request = it.request()
                it.proceed(request)
            })
            return builder.build()
        }
            private val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()

        fun getApi() = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(ok())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build().create(ApiService::class.java)
    }
}

