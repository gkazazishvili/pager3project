package com.example.viewpaginghw.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.example.viewpaginghw.ModelDataSource
import com.example.viewpaginghw.apiservice.ApiService

class MainViewModel(private val api:ApiService) : ViewModel() {


    fun loadUsers() = Pager(config = PagingConfig(pageSize = 1), pagingSourceFactory = {ModelDataSource(api)}).liveData.cachedIn(viewModelScope)
}