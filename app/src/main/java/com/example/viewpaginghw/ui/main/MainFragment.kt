package com.example.viewpaginghw.ui.main

import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpaginghw.Adapter
import com.example.viewpaginghw.LocationLoadingStateAdapter
import com.example.viewpaginghw.MainViewModelFactory
import com.example.viewpaginghw.apiservice.ApiService
import com.example.viewpaginghw.databinding.HandleLoadingBinding
import com.example.viewpaginghw.databinding.MainFragmentBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainFragment() : BaseFragment<MainFragmentBinding>(
    MainFragmentBinding::inflate) {


    lateinit var viewMo:MainViewModel

    lateinit var adapter:Adapter
    lateinit var locationLoadingAdapter:LocationLoadingStateAdapter
    private var handleLoadingBinding: HandleLoadingBinding? = null

    override fun start() {
        setupRecycler()
        fetchData()
    }

    private fun setupRecycler() {

    }

    private fun fetchData() {

        viewMo = ViewModelProvider(this, MainViewModelFactory(ApiService.getApi()))[MainViewModel::class.java]
        binding.rv.layoutManager = LinearLayoutManager(requireContext())
        adapter = Adapter(this)
        binding.rv.adapter = adapter


        lifecycleScope.launchWhenCreated{
            viewMo.loadUsers().observe(viewLifecycleOwner,{
                adapter.submitData(lifecycle, it)
                binding.swipe.setOnRefreshListener {
                    adapter.refresh()
                }
//                adapter.withLoadStateHeaderAndFooter(
//                    header = LocationLoadingStateAdapter(binding.rv.adapter as Adapter),
//                    footer = LocationLoadingStateAdapter(binding.rv.adapter as Adapter)
//                )


                lifecycleScope.launch {
                    adapter.loadStateFlow.collectLatest { loadStates ->
                        handleLoadingBinding?.progressBar?.isVisible = loadStates.refresh is LoadState.Loading
                        handleLoadingBinding?.retryButton?.isVisible = loadStates.refresh !is LoadState.Loading
                        handleLoadingBinding?.errorMsg?.isVisible = loadStates.refresh is LoadState.Error
                    }
                }

            })
        }

    }

}