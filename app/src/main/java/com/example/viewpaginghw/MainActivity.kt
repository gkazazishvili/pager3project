package com.example.viewpaginghw

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.viewpaginghw.databinding.MainActivityBinding
import com.example.viewpaginghw.ui.main.MainFragment
import java.lang.reflect.Array.newInstance

class MainActivity : AppCompatActivity() {

    private lateinit var binding: MainActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}